# README

## Objectives

* To run [Kaldi], an Automatic Speech Recognition system (ASR), client-side, in a web-browser
* To provide users with the following [use cases][typical_usage]:
  * "I am happy with the provided web interface but I would like to use my own models"
  * "I would like to change the user interface"
  * "I would like to compile to Web Assembly (WASM) differently"

The [emscripten] toolkit is used to cross-compile to WASM.

**Note**: Depending on your specific use case, you may not need to compile Kaldi to WASM.

## What we provide

* Docker images and a docker-compose file to run the application
* Pre-compiled WASM binaries to run Kaldi in webworkers
* Source code for our web interface, which also serves as an example of how to
  use the C++ classes exported to Javascript.
* Build scripts to cross-compile Kaldi and its dependencies to WASM.
* A [documentation] about the project which, among other things, describes
  * How to get started
  * How the project is structured
  * What the requirements for the ASR models are
  * How to compile the project

The application provided in this repo can be viewed here:  
https://kaldi-web.loria.fr

## Contributing

The javascript code uses the Airbnb style mostly. Additionally lines are limited
to 80 characters and 2 spaces are used for indentation.  
The complete specifications are under `.eslintrc.js`. Please run `npm run lint`
to run eslint.

The frontend framework is [React].

[documentation]: https://gitlab.inria.fr/kaldi.web/kaldi-wasm/-/wikis/home
[emscripten]: https://emscripten.org/
[Kaldi]: https://github.com/kaldi-asr/kaldi
[React]: https://reactjs.org/
[typical_usage]: https://gitlab.inria.fr/kaldi.web/kaldi-wasm/-/wikis/typical_usage.md
